FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/wallet
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/wallet /go/src/wallet


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/wallet /usr/local/bin/wallet
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["wallet"]
