/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type PaginationParams struct {
	PageLimit  uint64 `json:"page-limit"`
	PageNumber uint64 `json:"page-number"`
	PageOrder  string `json:"page-order"`
}
