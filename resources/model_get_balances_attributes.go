/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type GetBalancesAttributes struct {
	Addresses  []string `json:"addresses"`
	PageLimit  uint64   `json:"page-limit"`
	PageNumber uint64   `json:"page-number"`
	PageOrder  string   `json:"page-order"`
	Tokens     []string `json:"tokens"`
}
