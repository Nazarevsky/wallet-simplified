/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type NetworkAttributes struct {
	BlockGenTime int64  `json:"block-gen-time"`
	BlockSize    int64  `json:"block-size"`
	Consensus    string `json:"consensus"`
	Protocol     string `json:"protocol"`
}
