/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type GetBalances struct {
	Key
	Attributes GetBalancesAttributes `json:"attributes"`
}
type GetBalancesResponse struct {
	Data     GetBalances `json:"data"`
	Included Included    `json:"included"`
}

type GetBalancesListResponse struct {
	Data     []GetBalances `json:"data"`
	Included Included      `json:"included"`
	Links    *Links        `json:"links"`
}

// MustGetBalances - returns GetBalances from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustGetBalances(key Key) *GetBalances {
	var getBalances GetBalances
	if c.tryFindEntry(key, &getBalances) {
		return &getBalances
	}
	return nil
}
