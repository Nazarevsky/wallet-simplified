/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	BALANCE  ResourceType = "balance"
	BALANCES ResourceType = "balances"
	NETWORK  ResourceType = "network"
)
