package data

import "gitlab.com/distributed_lab/kit/pgdb"

type WalletQ interface {
	New() WalletQ
	GetWallets(netId int64, pageParams pgdb.OffsetPageParams) ([]Wallet, error)
}

type Wallet struct {
	Address   string `db:"address"`
	NetworkId int64  `db:"network_id"`
	Balance   uint64 `db:"balance"`
}
