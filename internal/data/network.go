package data

type NetworkQ interface {
	New() NetworkQ
	GetById(id int64) (Network, error)
}

type Network struct {
	Id           int64  `db:"id"`
	BlockSize    int64  `db:"block_size"`
	BlockGenTime int64  `db:"block_gen_time"`
	Consensus    string `db:"consensus"`
	Protocol     string `db:"protocol"`
}
