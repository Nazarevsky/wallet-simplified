package pg

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"wallet/internal/data"
)

const networkTableName = "Network"

func NewNetworkQ(db *pgdb.DB) data.NetworkQ {
	return &networkQ{db: db.Clone()}
}

type networkQ struct {
	db *pgdb.DB
}

func (n *networkQ) GetById(id int64) (data.Network, error) {
	var network data.Network

	idCheck := sq.Eq{"id": id}
	err := n.db.Get(&network, sq.Select("*").From(networkTableName).Where(idCheck))

	if err == sql.ErrNoRows {
		return data.Network{}, nil
	}

	return network, err
}

func (n *networkQ) New() data.NetworkQ {
	return NewNetworkQ(n.db)
}
