package pg

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"wallet/internal/data"
)

const walletTableName = "Wallet"

func NewWalletQ(db *pgdb.DB) data.WalletQ {
	return &walletQ{db: db.Clone()}
}

type walletQ struct {
	db *pgdb.DB
}

func (w walletQ) GetWallets(netId int64, pageParams pgdb.OffsetPageParams) ([]data.Wallet, error) {
	var result []data.Wallet

	idCheck := sq.Eq{"network_id": netId}
	sqlQuery := sq.Select("*").From(walletTableName).Where(idCheck)
	sqlQuery = pageParams.ApplyTo(sqlQuery, "address")

	err := w.db.Get(&result, sqlQuery)

	if err == sql.ErrNoRows {
		return []data.Wallet{}, nil
	}

	return result, err
}

func (w walletQ) New() data.WalletQ {
	return NewWalletQ(w.db)
}
