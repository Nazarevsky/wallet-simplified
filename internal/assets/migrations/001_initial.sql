-- +migrate Up
CREATE TABLE Network(
   id bigserial PRIMARY KEY,
   block_size bigint not null,
   block_gen_time bigint not null,
   consensus varchar(30) not null,
   protocol varchar(30) not null
);

CREATE TABLE Wallet(
   address varchar(100) PRIMARY KEY,
   network_id bigint NOT NULL,
   balance bigint NOT NULL,
   FOREIGN KEY (network_id) REFERENCES Network(id) ON DELETE CASCADE
);

-- +migrate Down
DROP TABLE Wallet;
DROP TABLE Network;