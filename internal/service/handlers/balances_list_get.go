package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/kit/pgdb"
	"net/http"
	"wallet/internal/data"
	"wallet/internal/service/helpers"
	"wallet/internal/service/requests"
	"wallet/resources"
)

func GetBalancesList(w http.ResponseWriter, r *http.Request) {
	netId, err := requests.GetNetworkRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	request, err := requests.GetBalancesRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	offsetPageParams := pgdb.OffsetPageParams{
		Limit:      request.Attributes.PageLimit,
		Order:      request.Attributes.PageOrder,
		PageNumber: request.Attributes.PageNumber,
	}

	wallets, err := WalletQ(r).GetWallets(netId, offsetPageParams)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	ape.Render(w, resources.BalanceListResponse{
		Data:  formBalances(wallets),
		Links: helpers.GetOffsetLinks(r, offsetPageParams),
	})
}

func formBalances(wallets []data.Wallet) []resources.Balance {
	var result []resources.Balance

	var i int64
	for ; i < int64(len(wallets)); i++ {
		result = append(result, resources.Balance{
			Key: resources.NewKeyInt64(i, resources.BALANCE),
			Attributes: resources.BalanceAttributes{
				Value: wallets[i].Balance,
			},
		})
	}

	return result
}
