package handlers

import (
	"context"
	"net/http"
	"wallet/internal/data"

	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	networkQCtxKey
	walletQCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxNetworkQ(entry data.NetworkQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, networkQCtxKey, entry)
	}
}

func NetworkQ(r *http.Request) data.NetworkQ {
	return r.Context().Value(networkQCtxKey).(data.NetworkQ).New()
}

func CtxWalletQ(entry data.WalletQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, walletQCtxKey, entry)
	}
}

func WalletQ(r *http.Request) data.WalletQ {
	return r.Context().Value(walletQCtxKey).(data.WalletQ).New()
}
