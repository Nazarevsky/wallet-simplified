package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
	"wallet/internal/service/requests"
	"wallet/resources"
)

func GetNetwork(w http.ResponseWriter, r *http.Request) {
	netId, err := requests.GetNetworkRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	network, err := NetworkQ(r).GetById(netId)

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	ape.Render(w, resources.NetworkResponse{
		Data: resources.Network{
			Key: resources.NewKeyInt64(netId, resources.NETWORK),
			Attributes: resources.NetworkAttributes{
				BlockGenTime: network.BlockGenTime,
				BlockSize:    network.BlockSize,
				Consensus:    network.Consensus,
				Protocol:     network.Protocol,
			},
		},
	})
}
