package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"wallet/internal/data/pg"
	"wallet/internal/service/handlers"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxNetworkQ(pg.NewNetworkQ(s.db)),
		),
	)
	r.Route("/network", func(r chi.Router) {
		r.Get("/{network_id}", handlers.GetNetwork)
		r.Post("/{network_id}", handlers.GetBalancesList)
	})

	return r
}
