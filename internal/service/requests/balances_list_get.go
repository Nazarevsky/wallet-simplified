package requests

import (
	"encoding/json"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
	"wallet/internal/service/helpers"
	"wallet/resources"
)

func GetBalancesRequest(r *http.Request) (resources.GetBalances, error) {
	var balancesRequest resources.GetBalances

	if err := json.NewDecoder(r.Body).Decode(&balancesRequest); err != nil {
		return balancesRequest, errors.Wrap(err, "failed to unmarshal")
	}

	return balancesRequest, validateGetBalancesRequest(balancesRequest)
}

func validateGetBalancesRequest(gb resources.GetBalances) error {
	return helpers.MergeErrors(validation.Errors{
		"/attributes/tokens": validation.Validate(gb.Attributes.Tokens,
			validation.Required),
		"/attributes/addresses": validation.Validate(gb.Attributes.Addresses,
			validation.Required, validation.Each(validation.Length(1, 100))),
		"/attributes/pagination_params/page_number": validation.Validate(gb.Attributes.PageNumber,
			validation.Required, validation.Min(1)),
		"/attributes/pagination_params/page_limit": validation.Validate(gb.Attributes.PageLimit,
			validation.Required, validation.Min(1)),
		"/attributes/pagination_params/page_order": validation.Validate(gb.Attributes.PageOrder,
			validation.Required, validation.Length(3, 4)),
	}).Filter()
}
