package requests

import (
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

func GetNetworkRequest(r *http.Request) (int64, error) {
	id, err := strconv.ParseInt(chi.URLParam(r, "network_id"), 10, 64)

	if err != nil {
		return -1, err
	}

	return id, err
}
